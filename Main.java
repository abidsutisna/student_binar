package com.tutorial;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);

            printMenu();
            int menu = userInput.nextInt();

        switch (menu){
            //Luas
            case 1:
                printBidangLuas();
                int bidangLuas = userInput.nextInt();

                //persegi
                if (bidangLuas == 1){
                    printPersegi();
                    System.out.print("masukkan sisi = ");
                    double sisiPersegi = userInput.nextDouble();
                    double luasPersegi = sisiPersegi * sisiPersegi;
                    System.out.println("Luas persegi adalah = " + luasPersegi);
                //lingkaran
                } else if (bidangLuas == 2){
                    printLingkaran();
                    System.out.print("Masukkan jari-jari = ");
                    double jariLingkaran = userInput.nextDouble();
                    double luasLingkaran = 3.14 * jariLingkaran * jariLingkaran;
                    System.out.println("Luas lingkaran adalah = " + luasLingkaran);
                //segitiga
                } else if (bidangLuas == 3){
                    printSegitiga();
                    System.out.print("masukkan alas = ");
                    double alasSegitiga = userInput.nextDouble();
                    System.out.print("masukkan tinggi = ");
                    double tinggiSegitiga = userInput.nextDouble();
                    double luasSegitiga = (alasSegitiga*tinggiSegitiga)/2;
                    System.out.println("luas segitiga adalah = " + luasSegitiga);
                //persegi panjang
                } else if (bidangLuas == 4){
                    printPersegiPanjang();
                    System.out.print("masukkan panjang = ");
                    double panjangPersegiPanjang = userInput.nextDouble();
                    System.out.print("masukkan lebar = ");
                    double lebarPersegiPanjang = userInput.nextDouble();
                    double luasPersegiPanjang = panjangPersegiPanjang*lebarPersegiPanjang;
                    System.out.println("luas persegi panjang adalah = " + luasPersegiPanjang);
                } else if (bidangLuas == 0){
                    break;
                } else {
                    System.out.println("PILIHAN TIDAK ADA");
                }
                break;
            //volum
            case 2:
                printBidangVolum();
                int bidangVolum = userInput.nextInt();
                //kubus
                if (bidangVolum == 1){
                    printKubus();
                    System.out.print("masukkan sisi = ");
                    double sisiKubus = userInput.nextDouble();
                    double volumKubus = sisiKubus*sisiKubus*sisiKubus;
                    System.out.println("volum kubus adalah = " + volumKubus);
                //balok
                } else if (bidangVolum == 2){
                    printBalok();
                    System.out.print("masukkan panjang = ");
                    double pangjangBalok = userInput.nextDouble();
                    System.out.print("masukkan tinggi = ");
                    double tinggiBalok = userInput.nextDouble();
                    System.out.print("masukkan lebar = ");
                    double lebarBalok = userInput.nextDouble();
                    double volumBalok = pangjangBalok*tinggiBalok*lebarBalok;
                    System.out.println("volum balok adalah = " + volumBalok);
                //tabung
                } else if (bidangVolum == 3){
                    printTabung();
                    System.out.print("masukkan tinggi = ");
                    double tinggiTabung = userInput.nextDouble();
                    System.out.print("masukkan jari-jari tabung = ");
                    double jariTabung = userInput.nextDouble();
                    double volumTabung = 3.14 * tinggiTabung * jariTabung * jariTabung;
                    System.out.println("volum tabung adalah = " + volumTabung);
                }
                break;
            case 0:
                break;
            default:
                System.out.println("PILIHAN TIDAK ADA");
        }
    }

    public static void printMenu(){
        System.out.println("---------------------------------------");
        System.out.println("Kalkulator Penghitung Luas dan Volum");
        System.out.println("---------------------------------------");
        System.out.println("Menu");
        System.out.println("1. Hitung Luas Bidang");
        System.out.println("2. Hitung Volum");
        System.out.println("0. Tutup aplikasi");
        System.out.println("---------------------------------------");
        System.out.print("Masukkan pilihan menu = ");
    }
    public static void printBidangLuas(){
        System.out.println("---------------------------------------");
        System.out.println("Pilih bidang yang akan dihitung");
        System.out.println("---------------------------------------");
        System.out.println("1. Persegi");
        System.out.println("2. Lingkaran");
        System.out.println("3. Segitiga");
        System.out.println("4. Persegi Panjang");
        System.out.println("0. Kembali ke menu sebelumnya");
        System.out.println("---------------------------------------");
        System.out.print("Masukkan pilihan bidang = ");
    }
    public static void printBidangVolum(){
        System.out.println("---------------------------------------");
        System.out.println("Pilih bidang yang akan dihitung");
        System.out.println("---------------------------------------");
        System.out.println("1. kubus");
        System.out.println("2. Balok");
        System.out.println("3. Tabung");
        System.out.println("0. Kembali ke menu sebelumnya");
        System.out.println("---------------------------------------");
        System.out.print("Masukkan pilihan bidang = ");
    }
    public static void printPersegi(){
        System.out.println("=======================================");
        System.out.println("Anda memilih persegi");
        System.out.println("=======================================");
    }
    public static void printLingkaran(){
        System.out.println("=======================================");
        System.out.println("Anda memilih Lingkaran");
        System.out.println("=======================================");
    }
    public static void printSegitiga(){
        System.out.println("=======================================");
        System.out.println("Anda memilih Segitiga");
        System.out.println("=======================================");
    }
    public static void printPersegiPanjang(){
        System.out.println("=======================================");
        System.out.println("Anda memilih persegi panjang");
        System.out.println("=======================================");
    }
    public static void printKubus(){
        System.out.println("=======================================");
        System.out.println("Anda memilih Kubus");
        System.out.println("=======================================");
    }
    public static void printBalok(){
        System.out.println("=======================================");
        System.out.println("Anda memilih Balok");
        System.out.println("=======================================");
    }
    public static void printTabung(){
        System.out.println("=======================================");
        System.out.println("Anda memilih Tabung");
        System.out.println("=======================================");
    }

}
